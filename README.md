JavaScript Training
Note: This Traning is only about JavaScript Concepts. its not specific to any environment. All these concepts should apply all browsers and NodeJS
Beginner: estimated 2-3 days required
Basic Data Types - primitive: [String, Number, Boolean, Object, Undefined], non-primitive: [Object, Array, RegExp]
http://www.w3schools.com/js/js_datatypes.asp
Control Flows - if condition, ternary operator, while loops, for loops, switch
Operators - Arithmetic, Assignment, String, Comparison, Conditional, Logical, Bitwise
http://www.w3schools.com/jsref/jsref_operators.asp
What is NaN ?
Falsy values - false, 0, "", null, undefined,
Use of typeof, instanceof
Use of var
Use Logical Operators instead of if conditions.
Explore Nativa Methods of Array - forEach, find, map, sort, length, push, shift, indexOf, concat, join etc.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
Explore String Methods - split, splice etc.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
Explore Date Methods
https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Date
Explore Math Methods
https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Math
Timers - setTimeout, setInterval, clearTimeout
Explore Ways of creating objects
http://a-developer-life.blogspot.in/2011/11/7-ways-to-create-objects-in-javascript.html
Intermediate: estimated 4-5 days required
Scope and context
http://www.w3schools.com/js/js_scope.asp
http://ryanmorr.com/understanding-scope-and-context-in-javascript/
http://stackoverflow.com/a/1484230
closures
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures
Immediately - invoked function expression and its benefits
OOJS (Object Orieinted JavaScript)
Classes using constructor functions
What is this ?
Prototype - __ proto __, Fn.prototype
'use strict' - Strict Mode;
Variable Hoisting
Function Hoisting
Advanced: estimated 4-5 days required
ES6 Basics
let , const, enum
ES6 class, constuctors
Promises
Template String
import and export
diff b/w arrow Function and Normal Function
Enhanced Object Literals
ES6 to ES5 - Conversions manually
Expert: estimated 4-5 days required
ES6
generators
ES7
Decorator